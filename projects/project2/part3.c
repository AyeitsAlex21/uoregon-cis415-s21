#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "string_parser.h"

void script_print (pid_t* pid_ary, int size)
{
	FILE* fout;
	fout = fopen ("top_script.sh", "w");
	fprintf(fout, "#!/bin/bash\ntop");
	for (int i = 0; i < size; i++)
	{
		fprintf(fout, " -p %d", (int)(pid_ary[i]));
	}
	fprintf(fout, "\n");
	fclose (fout);

	char* top_arg[] = {"gnome-terminal", "--", "bash", "top_script.sh", NULL};
	pid_t top_pid;

	top_pid = fork();
	{
		if (top_pid == 0)
		{
			if(execvp(top_arg[0], top_arg) == -1)
			{
				perror ("top command: ");
			}
			printf("%d\n", top_pid);
			exit(0);
		}
	}
}

int main(int argc, char *argv[])
{
    char *buf = NULL;
    size_t len = 0;
    FILE *instream;
    int opt = -1;
    int no_file = 1;
    command_line large_token_buffer;

    while((opt = getopt(argc, argv, "f")) != -1)
    {
        switch(opt)
        {
            case 'f': 
                if( optind < argc && optind + 1 == argc)
                {
                    instream = fopen(argv[optind], "r+");
                    if(instream == NULL)
                    {
                        write(1, "Invalid use: File does not exist\n", 33);
                        exit(1);
                    }
                    no_file = 0;
                }
                else
                {
                    write(1, "Invalid use: incorrect number of parameters\n", 44);
                    exit(1);
                }
                break;

            case '?': exit(1); break;
        }
    }

    // no flag specified
    if(no_file == 1)
    {
        write(1, "No file inputed\n", strlen("No file inputed\n"));
        exit(EXIT_FAILURE);
    }


    //freopen("output.txt", "w+", stdout);

    int line_number = 0;
    int size = 50;
    pid_t *pid_ary = (pid_t *)malloc(sizeof(pid_t) * size);

    int sig;
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGCONT);
    sigaddset(&sigset, SIGALRM);
    sigprocmask(SIG_BLOCK, &sigset, NULL);

    while (getline (&buf, &len, instream) != -1)
    {
        large_token_buffer = str_filler (buf, " ");

        // dont know how big input is so reallocate when limit hit
        if(line_number >= size)
        {
            size *= 2;
            pid_ary = (pid_t *)realloc(pid_ary, sizeof(pid_t) * size);
        }

        // create child process
        pid_ary[line_number] = fork();
        if(pid_ary[line_number] < 0)
        {
            perror("fork"); // error handling
        }

        // if child execute line
        if(pid_ary[line_number] == 0)
        {
            sigwait(&sigset, &sig);
            // wait for signal before executing the command
            if(execvp(large_token_buffer.command_list[0], 
                    large_token_buffer.command_list) == -1)
            {
                perror("Execvp"); // error handling
            }

            // free child process memory if failed to execute
            free(buf);
            fclose(instream);
            free_command_line(&large_token_buffer);
            free(pid_ary);
            exit(-1);
        }
        // get ready for next line
        free_command_line (&large_token_buffer);
		memset (&large_token_buffer, 0, 0);
        line_number++;
    }

    script_print(pid_ary, line_number);
    int i = 0;
    int num_process = line_number;
    int wstatus;

    while(1)
    {
        // waitpid() with WNOHANG returns the pid of the child
        // if reaped succesfully 0 otherwise
        if(waitpid(pid_ary[i], &wstatus, WNOHANG) > 0)
        {
            if(WIFEXITED(wstatus))
            {
                num_process--;
                pid_ary[i] = -1;
            }
            
            if(num_process == 0)
                break;
        }
        else
        {
            if(kill(pid_ary[i], SIGCONT) == -1)
                perror("SIGCONT ");

            alarm(1);
            sigwait(&sigset, &sig);

            if(kill(pid_ary[i], SIGSTOP) == -1)
                perror("SIGSTOP ");
        }

        // find the next process that has not terminated yet
        do{ i = (i + 1) % line_number;
        }while (pid_ary[i] == -1);
    }

    if(instream != NULL)
        fclose(instream);
    if(buf != NULL)
        free(buf);
    free(pid_ary);
    
    exit(EXIT_SUCCESS);
    return EXIT_SUCCESS;
}