#define _GNU_SOURCE
#include "account.h"
#include "string_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <syscall.h>

#define gettid() ((pid_t)syscall(SYS_gettid))
#define MAX_THREAD 10

void* process_transaction(void *arg);
void* update_balance(void *arg);
void deposit(int index, command_line *cmd);
void withdraw(int index, command_line *cmd);
void transfer_funds(int index1, int index2, command_line *cmd);
void check_balance(int index);
int search_account(char* account_number, char* password);
void* process_thread(void *arg);

account **accounts;
pthread_t tid[MAX_THREAD];

pthread_barrier_t barrier;// barrier sycnro object

pthread_mutex_t num_completed_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t worker_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t bank_lock = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t bank = PTHREAD_COND_INITIALIZER; // to signal bank thread
pthread_cond_t worker = PTHREAD_COND_INITIALIZER; // to signal bank thread

unsigned int num_current_threads;
unsigned int waiting_thread_count = 0;
long long num_completed = 0; 
unsigned long num_commands = 0;
long long update_threshold = 5000;
int done = 0;

unsigned int valid_requests = 0;

int main(int argc, char *argv[])
{
    FILE *instream;

    if(argc != 2)
    {
        printf("Invalid use: incorrect number of parameters\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        instream = fopen(argv[1], "r");
        if(instream == NULL)
        {
            printf("Invalid use: File does not exist\n");
            exit(EXIT_FAILURE);
        }
    }

    char *buf = NULL;
    size_t len = 0;
    int num_accounts = 0;
    int ctr = 0;
    command_line cmd;

    getline(&buf, &len, instream);
    sscanf(buf, "%d", &num_accounts);

    accounts = (account **) malloc(sizeof(account *) * (num_accounts + 1));

    // make an OUTPUT directory
    struct stat st = {0};
    if(stat("output", &st) == -1)
        mkdir("output", S_IRWXU);

    for(int i = 0; i < num_accounts; i++)
        accounts[i] = (account *) malloc(sizeof(account));
    accounts[num_accounts] = NULL;
    
    while(num_accounts > ctr)
    {
        // attain account index
        int index;
        getline(&buf, &len, instream);
        cmd = str_filler(buf, " ");
        sscanf(cmd.command_list[1], "%d", &index);
        free_command_line(&cmd);

        // attain account number
        getline(&buf, &len, instream);
        sscanf(buf, "%s", (accounts[index]->account_number));

        // assign outputfile
        sprintf(accounts[index]->out_file, "output/%s.txt", accounts[index]->account_number);

        // setup the file
        FILE *tmp = fopen(accounts[index]->out_file, "w");
        fprintf(tmp, "account %d:\n", index);
        fclose(tmp);

        // password
        getline(&buf, &len, instream);
        sscanf(buf, "%s", (accounts[index]->password));

        // initial balance
        getline(&buf, &len, instream);
        sscanf(buf, "%lf", &(accounts[index]->balance));

        // reward rate
        getline(&buf, &len, instream);
        sscanf(buf, "%lf", &(accounts[index]->reward_rate));

        // initialize lock
        pthread_mutex_init(&(accounts[index]->ac_lock), NULL);

        accounts[index]->transaction_tracter = 0;
        // account registerd
        ctr++;
    }

    unsigned long cmd_max_size = 25;
    command_line *cmd_arr = (command_line*)malloc(sizeof(command_line)*cmd_max_size);
    // count number of requests and save requests
    while(getline(&buf, &len, instream) != -1)
    {
        if(num_commands >= cmd_max_size)
        {
            cmd_max_size *= 2;
            cmd_arr = realloc(cmd_arr, sizeof(command_line) * cmd_max_size);
        }
        cmd_arr[num_commands] = str_filler(buf, " ");
        num_commands++;
    }

    // initialize the barrier
    int ret = pthread_barrier_init(&barrier, NULL, MAX_THREAD);
    if(ret != 0)
    {
        printf("Failed to initialize barrier exiting\n");
        exit(EXIT_FAILURE);
    }

    // call banker thread and make it wait in function
    pthread_mutex_lock(&bank_lock);
    pthread_t bank_tid;
    pthread_create(&bank_tid, NULL, &update_balance, NULL);

    // wait for bank thread to cond wait
    pthread_cond_wait(&bank, &bank_lock);
    pthread_mutex_unlock(&bank_lock);

    num_current_threads = MAX_THREAD;
    // create and assign threads
    for(unsigned long i = 0; i < MAX_THREAD; i++)
    {
        unsigned long offset = i * (num_commands/MAX_THREAD);
        int error = pthread_create(&(tid[i]), NULL, &process_thread,(cmd_arr+offset));
        if(error != 0)
            printf("\nThread can't be created :[%s]", strerror(error));
    }
    // wait for threads
    for(int i = 0; i < MAX_THREAD; i++)
        pthread_join(tid[i], NULL);


    // signal bank thread to do one last upadate and then exit
    printf("main about to signal bank\n");

    // make sure bank thread makes it to the condwait
    // before making it exit
    pthread_mutex_lock(&bank_lock);
    done = 1;
    pthread_cond_signal(&bank);
    pthread_mutex_unlock(&bank_lock);
    pthread_join(bank_tid, NULL);

    // put final results in output.txt
    FILE *temp = fopen("output.txt", "w");
    if(temp != NULL){
        for(int i = 0; i < num_accounts; i++)
            fprintf(temp, "%d balance:	%.2lf\n\n", i, accounts[i]->balance);
        fclose(temp);
    }

    // free stuff
    if(cmd_arr != NULL)
    {
        free(cmd_arr);
    }
    if(buf != NULL)
        free(buf);
    if(accounts != NULL)
    {
        for(int i = 0; i < num_accounts; i++)
            free(accounts[i]);
        free(accounts);
    }
    fclose(instream);

    pthread_mutex_destroy(&bank_lock);
    pthread_mutex_destroy(&worker_lock);
    pthread_mutex_destroy(&num_completed_lock);
    pthread_barrier_destroy(&barrier);
    pthread_cond_destroy(&bank);
    pthread_cond_destroy(&worker);

    return EXIT_SUCCESS;
}

void* process_thread(void *arg)
{
    // every thread has to wait until all threads are created
    // and reach barrier wait to start executing together
    pthread_barrier_wait(&barrier);

    command_line *cmd_arr = (command_line *) arg;
    unsigned long num_requests = num_commands / MAX_THREAD;

    // give the remainder of requests to last thread
    if(tid[MAX_THREAD - 1] == pthread_self()) 
        num_requests += num_commands % MAX_THREAD;
    
    // NOTE
    // give the remainder of requests to last thread
    // biggest reamainder with 10 threads is 9 so
    // last thread could have at most 9 more things to do
    // then the other threads

    for(unsigned long i = 0; i < num_requests; i++)
    {
        process_transaction((void *) &(cmd_arr[i]));
        free_command_line(&(cmd_arr[i]));

        if(num_completed >= update_threshold)
        {
            pthread_mutex_lock(&worker_lock);
            waiting_thread_count++;
            
            if(waiting_thread_count >= num_current_threads)
            {
                pthread_mutex_unlock(&worker_lock);
                pthread_mutex_lock(&bank_lock);

                printf("%d signaling bank thread\n", gettid());
                pthread_cond_signal(&bank); // signal bank 
                printf("%d number reached, pausing\n", gettid());

                // realease bank lock and wait for worker signal
                pthread_cond_wait(&worker, &bank_lock);
                pthread_mutex_unlock(&bank_lock);
            }
            else
            {
                printf("%d number reached, pausing\n", gettid());
                pthread_cond_wait(&worker, &worker_lock); 
                pthread_mutex_unlock(&worker_lock);
            }
        }
    }

    pthread_mutex_lock(&worker_lock);
    num_current_threads--;


    if(num_completed >= update_threshold && 
        waiting_thread_count >= num_current_threads)
    {
        printf("%d signaling bank thread\n", gettid());

        pthread_mutex_lock(&bank_lock);
        pthread_cond_signal(&bank);
        pthread_mutex_unlock(&bank_lock);
    }
    pthread_mutex_unlock(&worker_lock);

    printf("%d is done\n", gettid());
    
    return NULL;
}

void* process_transaction(void *arg)
{
    command_line *command = (command_line *) arg;
    char *password = command->command_list[2];
    char *account_number1 = command->command_list[1];
    char *account_number2;
    int index1 = search_account(account_number1, password);
    
    if(index1 != -1)
    {
        if(strcmp(command->command_list[0], "T") == 0)
        {
            account_number2 = command->command_list[3];
            int index2 = search_account(account_number2, NULL);
            if(index2 != -1)
            {
                transfer_funds(index1, index2, command);

                pthread_mutex_lock(&num_completed_lock);
                num_completed++;
                valid_requests++;
                pthread_mutex_unlock(&num_completed_lock);
            }
        }
        else if(strcmp(command->command_list[0], "D") == 0)
        {
            deposit(index1, command);

            pthread_mutex_lock(&num_completed_lock);
            num_completed++;
            valid_requests++;
            pthread_mutex_unlock(&num_completed_lock);
        }
        else if(strcmp(command->command_list[0], "W") == 0)
        {
            withdraw(index1, command);

            pthread_mutex_lock(&num_completed_lock);
            num_completed++;
            valid_requests++;
            pthread_mutex_unlock(&num_completed_lock);
        }
        else if(strcmp(command->command_list[0], "C") == 0)
            check_balance(index1);
    }
    return NULL;
}

void* update_balance(void *arg)
{
    unsigned int num_updated = 0;
    pthread_mutex_lock(&bank_lock);
    pthread_cond_signal(&bank); // signal main thread

    while(1)
    {
        // realease bank lock and wait
        pthread_cond_wait(&bank, &bank_lock);

        // bank thread exits if no more transactions to process
        // if there is still a non-zero value in an account's
        // transaction tracter skip this statement exit after
        // update
        if(done == 1 && num_completed <= 0)
        {
            printf("total updates %u\n", num_updated);
            pthread_exit(NULL);
        }

        printf("bank %d signal recieved\n", gettid());

        for(int i = 0; accounts[i] != NULL; i++)
        {
            accounts[i]->balance += 
                ((accounts[i]->reward_rate) * accounts[i]->transaction_tracter);
            // reset the tracter
            accounts[i]->transaction_tracter = 0;
            FILE *outstream = fopen(accounts[i]->out_file, "a");
            if(outstream != NULL)
            {
                fprintf(outstream, "Current Balance:\t%.2lf\n", accounts[i]->balance);
                fclose(outstream);
            }
        }
        // broadcast to all threads and minus 5000 from current run of 
        // requests
        num_completed -= update_threshold;
        num_updated++;
        waiting_thread_count = 0;
        pthread_cond_broadcast(&worker);

        if(done == 1 && num_completed <= 0)
        {
            printf("total updates %u\n", num_updated);
            pthread_exit(NULL);
        }
        
        printf("bank %d waiting now, update time is %u\n", gettid(), num_updated);
    }
    return arg;
}

int search_account(char *account_number, char *password)
{
    int index = -1;

    for(int i = 0; accounts[i] != NULL; i++)
    {
        if(strcmp(account_number, accounts[i]->account_number) == 0)
        {
            if(password == NULL)
            {// if account num found but NULL password give index anyway
                index = i;
                break;
            }
            else
                if(strcmp(password, accounts[i]->password) == 0)
                {// if account num and password is found return index
                    index = i;
                    break;
                }
        }
    }
    return index;
}

void check_balance(__attribute__((unused)) int index)
{
    // was told not do anything in this function by Grayson
    //printf("%d balance:\t%.2lf\n", index, accounts[index]->balance);
}

void withdraw(int index, command_line *cmd)
{
    double withdraw_amount;
    sscanf(cmd->command_list[3],"%lf", &withdraw_amount);

    // critical section add mutex lock and unlock
    pthread_mutex_lock(&(accounts[index]->ac_lock));
    accounts[index]->balance -= withdraw_amount;
    accounts[index]->transaction_tracter += withdraw_amount;
    pthread_mutex_unlock(&(accounts[index]->ac_lock));
}

void deposit(int index, command_line *cmd)
{
    double dep_amount;
    sscanf(cmd->command_list[3],"%lf", &dep_amount);

    // critical section add mutex lock and unlock
    pthread_mutex_lock(&(accounts[index]->ac_lock));
    accounts[index]->balance += dep_amount;
    accounts[index]->transaction_tracter += dep_amount;
    pthread_mutex_unlock(&(accounts[index]->ac_lock));
}

void transfer_funds(int index1, int index2, command_line *cmd)
{
    double transfer_amount;
    sscanf(cmd->command_list[4],"%lf", &transfer_amount);

    // critical section add mutex lock and unlock but
    // seperate to avoid deadlocks

    pthread_mutex_lock(&(accounts[index2]->ac_lock));
    accounts[index2]->balance += transfer_amount; // give money to dest
    pthread_mutex_unlock(&(accounts[index2]->ac_lock));

    pthread_mutex_lock(&(accounts[index1]->ac_lock));
    accounts[index1]->balance -= transfer_amount; // take moiney from src
    accounts[index1]->transaction_tracter += transfer_amount; // add to src trans tracker
    pthread_mutex_unlock(&(accounts[index1]->ac_lock));
}