#include "account.h"
#include "string_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

void* process_transaction(void *arg);
void* update_balance(void *arg);
void deposit(int index, command_line *cmd);
void withdraw(int index, command_line *cmd);
void transfer_funds(int index1, int index2, command_line *cmd);
void check_balance(int index);
int search_account(char* account_number, char* password);

account **accounts;

int main(int argc, char *argv[])
{
    FILE *instream;

    if(argc != 2)
    {
        printf("Invalid use: incorrect number of parameters\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        instream = fopen(argv[1], "r");
        if(instream == NULL)
        {
            printf("Invalid use: File does not exist\n");
            exit(EXIT_FAILURE);
        }
    }

    char *buf = NULL;
    size_t len = 0;
    int num_accounts = 0;
    int ctr = 0;
    command_line cmd;

    getline(&buf, &len, instream);
    sscanf(buf, "%d", &num_accounts);

    accounts = (account **) malloc(sizeof(account *) * (num_accounts + 1));

    // make an OUTPUT directory
    struct stat st = {0};
    if(stat("output", &st) == -1)
        mkdir("output", S_IRWXU);

    for(int i = 0; i < num_accounts; i++)
        accounts[i] = (account *) malloc(sizeof(account));
    accounts[num_accounts] = NULL;
    
    while(num_accounts > ctr)
    {
        // attain account index
        int index;
        getline(&buf, &len, instream);
        cmd = str_filler(buf, " ");
        sscanf(cmd.command_list[1], "%d", &index);
        free_command_line(&cmd);

        // attain account number
        getline(&buf, &len, instream);
        sscanf(buf, "%s", (accounts[index]->account_number));

        // assign outputfile
        sprintf(accounts[index]->out_file, "output/%s.txt", accounts[index]->account_number);

        // setup the file
        FILE *tmp = fopen(accounts[index]->out_file, "w");
        fprintf(tmp, "account %d:\n", index);
        fclose(tmp);

        // password
        getline(&buf, &len, instream);
        sscanf(buf, "%s", (accounts[index]->password));

        // initial balance
        getline(&buf, &len, instream);
        sscanf(buf, "%lf", &(accounts[index]->balance));

        // reward rate
        getline(&buf, &len, instream);
        sscanf(buf, "%lf", &(accounts[index]->reward_rate));

        accounts[index]->transaction_tracter = 0;
        // account registerd
        ctr++;
    }

    while(getline(&buf, &len, instream) != -1)
    {
        cmd = str_filler(buf, " ");
        process_transaction((void *) &cmd); // you are here
        free_command_line(&cmd);
    }
    update_balance(NULL);

    // put final results in output.txt
    FILE *temp = fopen("output.txt", "w");
    if(temp != NULL){
        for(int i = 0; i < num_accounts; i++)
            fprintf(temp, "%d balance:	%.2lf\n\n", i, accounts[i]->balance);
        fclose(temp);
    }

    // free stuff
    if(buf != NULL)
        free(buf);
    if(accounts != NULL)
    {
        for(int i = 0; i < num_accounts; i++)
            free(accounts[i]);
        free(accounts);
    }
    fclose(instream);
}

void* process_transaction(void *arg)
{
    command_line *command = (command_line *) arg;
    char *password = command->command_list[2];
    char *account_number1 = command->command_list[1];
    char *account_number2;
    int index1 = search_account(account_number1, password);
    
    if(index1 != -1)
    {
        if(strcmp(command->command_list[0], "T") == 0)
        {
            account_number2 = command->command_list[3];
            int index2 = search_account(account_number2, NULL);
            if(index2 != -1)
                transfer_funds(index1, index2, command);
        }
        else if(strcmp(command->command_list[0], "D") == 0)
            deposit(index1, command);
        else if(strcmp(command->command_list[0], "W") == 0)
            withdraw(index1, command);
        else if(strcmp(command->command_list[0], "C") == 0)
            check_balance(index1);
    }
    return NULL;
}

void* update_balance(void *arg)
{
    for(int i = 0; accounts[i] != NULL; i++)
    {
        accounts[i]->balance += 
            ((accounts[i]->reward_rate) * accounts[i]->transaction_tracter);
        FILE *outstream = fopen(accounts[i]->out_file, "a");
        if(outstream != NULL)
        {
            fprintf(outstream, "Current Balance:\t%.2lf\n", accounts[i]->balance);
            fclose(outstream);
        }
    }
    return arg;
}

int search_account(char *account_number, char *password)
{
    int index = -1;

    for(int i = 0; accounts[i] != NULL; i++)
    {
        if(strcmp(account_number, accounts[i]->account_number) == 0)
        {
            if(password == NULL)
            {// if account num found but NULL password give index anyway
                index = i;
                break;
            }
            else
                if(strcmp(password, accounts[i]->password) == 0)
                {// if account num and password is found return index
                    index = i;
                    break;
                }
        }
    }
    return index;
}

void check_balance(__attribute__((unused)) int index)
{
    // was told not do anything in this function by Grayson
    //printf("%d balance:\t%.2lf\n", index, accounts[index]->balance);
}

void withdraw(int index, command_line *cmd)
{
    double withdraw_amount;
    sscanf(cmd->command_list[3],"%lf", &withdraw_amount);

    accounts[index]->balance -= withdraw_amount;
    accounts[index]->transaction_tracter += withdraw_amount;
}

void deposit(int index, command_line *cmd)
{
    double dep_amount;
    sscanf(cmd->command_list[3],"%lf", &dep_amount);

    accounts[index]->balance += dep_amount;
    accounts[index]->transaction_tracter += dep_amount;
}

void transfer_funds(int index1, int index2, command_line *cmd)
{
    double transfer_amount;
    sscanf(cmd->command_list[4],"%lf", &transfer_amount);

    accounts[index2]->balance += transfer_amount; // give money to dest
    accounts[index1]->balance -= transfer_amount; // take moiney from src
    accounts[index1]->transaction_tracter += transfer_amount; // add to src trans tracker
}