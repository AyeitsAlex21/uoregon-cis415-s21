#include "account.h"
#include "string_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#define MAX_THREAD 10

void* process_transaction(void *arg);
void* update_balance(void *arg);
void deposit(int index, command_line *cmd);
void withdraw(int index, command_line *cmd);
void transfer_funds(int index1, int index2, command_line *cmd);
void check_balance(int index);
int search_account(char* account_number, char* password);
void* process_thread(void *arg);

account **accounts;
unsigned long num_commands = 0;
pthread_t tid[MAX_THREAD];

int main(int argc, char *argv[])
{
    FILE *instream;

    if(argc != 2)
    {
        printf("Invalid use: incorrect number of parameters\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        instream = fopen(argv[1], "r");
        if(instream == NULL)
        {
            printf("Invalid use: File does not exist\n");
            exit(EXIT_FAILURE);
        }
    }

    char *buf = NULL;
    size_t len = 0;
    int num_accounts = 0;
    int ctr = 0;
    command_line cmd;

    getline(&buf, &len, instream);
    sscanf(buf, "%d", &num_accounts);

    accounts = (account **) malloc(sizeof(account *) * (num_accounts + 1));

    // make an OUTPUT directory
    struct stat st = {0};
    if(stat("output", &st) == -1)
        mkdir("output", S_IRWXU);

    for(int i = 0; i < num_accounts; i++)
        accounts[i] = (account *) malloc(sizeof(account));
    accounts[num_accounts] = NULL;
    
    while(num_accounts > ctr)
    {
        // attain account index
        int index;
        getline(&buf, &len, instream);
        cmd = str_filler(buf, " ");
        sscanf(cmd.command_list[1], "%d", &index);
        free_command_line(&cmd);

        // attain account number
        getline(&buf, &len, instream);
        sscanf(buf, "%s", (accounts[index]->account_number));

        // assign outputfile
        sprintf(accounts[index]->out_file, "output/%s.txt", accounts[index]->account_number);

        // setup the file
        FILE *tmp = fopen(accounts[index]->out_file, "w");
        fprintf(tmp, "account %d:\n", index);
        fclose(tmp);

        // password
        getline(&buf, &len, instream);
        sscanf(buf, "%s", (accounts[index]->password));

        // initial balance
        getline(&buf, &len, instream);
        sscanf(buf, "%lf", &(accounts[index]->balance));

        // reward rate
        getline(&buf, &len, instream);
        sscanf(buf, "%lf", &(accounts[index]->reward_rate));

        // initialize lock
        pthread_mutex_init(&(accounts[index]->ac_lock), NULL);

        accounts[index]->transaction_tracter = 0;
        // account registerd
        ctr++;
    }

    unsigned long cmd_max_size = 25;
    command_line *cmd_arr = (command_line*)malloc(sizeof(command_line)*cmd_max_size);
    // count number of requests and save requests
    while(getline(&buf, &len, instream) != -1)
    {
        if(num_commands >= cmd_max_size)
        {
            cmd_max_size *= 2;
            cmd_arr = realloc(cmd_arr, sizeof(command_line) * cmd_max_size);
        }
        cmd_arr[num_commands] = str_filler(buf, " ");
        num_commands++;
    }

    // create and assign threads
    for(unsigned long i = 0; i < MAX_THREAD; i++)
    {
        unsigned long offset = i * (num_commands/MAX_THREAD);
        int error = pthread_create(&(tid[i]), NULL, &process_thread,(cmd_arr+offset));
        if(error != 0)
            printf("\nThread can't be created :[%s]", strerror(error));
    }
    // wait for threads
    for(int i = 0; i < MAX_THREAD; i++)
        pthread_join(tid[i], NULL);

    // call banker thread and wait for it to be done
    pthread_t bank_tid;
    pthread_create(&bank_tid, NULL, &update_balance, NULL);
    pthread_join(bank_tid, NULL);

    // put final results in output.txt
    FILE *temp = fopen("output.txt", "w");
    if(temp != NULL){
        for(int i = 0; i < num_accounts; i++)
            fprintf(temp, "%d balance:	%.2lf\n\n", i, accounts[i]->balance);
        fclose(temp);
    }

    // free stuff
    if(cmd_arr != NULL)
    {
        free(cmd_arr);
    }
    if(buf != NULL)
        free(buf);
    if(accounts != NULL)
    {
        for(int i = 0; i < num_accounts; i++)
            free(accounts[i]);
        free(accounts);
    }
    fclose(instream);
}

void* process_thread(void *arg)
{
    command_line *cmd_arr = (command_line *) arg;
    unsigned long num_requests = num_commands / MAX_THREAD;

    // give the remainder of requests to last thread
    if(tid[MAX_THREAD - 1] == pthread_self()) 
        num_requests += num_commands % MAX_THREAD;
    
    // NOTE
    // give the remainder of requests to last thread
    // biggest reamainder with 10 threads is 9 so
    // last thread could have at most 9 more things to do
    // then the other threads

    for(unsigned long i = 0; i < num_requests; i++)
    {
        process_transaction((void *) &(cmd_arr[i]));
        free_command_line(&(cmd_arr[i]));
    }
    return NULL;
}

void* process_transaction(void *arg)
{
    command_line *command = (command_line *) arg;
    char *password = command->command_list[2];
    char *account_number1 = command->command_list[1];
    char *account_number2;
    int index1 = search_account(account_number1, password);
    
    if(index1 != -1)
    {
        if(strcmp(command->command_list[0], "T") == 0)
        {
            account_number2 = command->command_list[3];
            int index2 = search_account(account_number2, NULL);
            if(index2 != -1)
                transfer_funds(index1, index2, command);
        }
        else if(strcmp(command->command_list[0], "D") == 0)
            deposit(index1, command);
        else if(strcmp(command->command_list[0], "W") == 0)
            withdraw(index1, command);
        else if(strcmp(command->command_list[0], "C") == 0)
            check_balance(index1);
    }
    return NULL;
}

void* update_balance(void *arg)
{
    for(int i = 0; accounts[i] != NULL; i++)
    {
        accounts[i]->balance += 
            ((accounts[i]->reward_rate) * accounts[i]->transaction_tracter);
        FILE *outstream = fopen(accounts[i]->out_file, "a");
        if(outstream != NULL)
        {
            fprintf(outstream, "Current Balance:\t%.2lf\n", accounts[i]->balance);
            fclose(outstream);
        }
    }
    return arg;
}

int search_account(char *account_number, char *password)
{
    int index = -1;

    for(int i = 0; accounts[i] != NULL; i++)
    {
        if(strcmp(account_number, accounts[i]->account_number) == 0)
        {
            if(password == NULL)
            {// if account num found but NULL password give index anyway
                index = i;
                break;
            }
            else
                if(strcmp(password, accounts[i]->password) == 0)
                {// if account num and password is found return index
                    index = i;
                    break;
                }
        }
    }
    return index;
}

void check_balance(__attribute__((unused)) int index)
{
    // was told not do anything in this function by Grayson
    //printf("%d balance:\t%.2lf\n", index, accounts[index]->balance);
}

void withdraw(int index, command_line *cmd)
{
    double withdraw_amount;
    sscanf(cmd->command_list[3],"%lf", &withdraw_amount);

    // critical section add mutex lock and unlock
    pthread_mutex_lock(&(accounts[index]->ac_lock));
    accounts[index]->balance -= withdraw_amount;
    accounts[index]->transaction_tracter += withdraw_amount;
    pthread_mutex_unlock(&(accounts[index]->ac_lock));
}

void deposit(int index, command_line *cmd)
{
    double dep_amount;
    sscanf(cmd->command_list[3],"%lf", &dep_amount);

    // critical section add mutex lock and unlock
    pthread_mutex_lock(&(accounts[index]->ac_lock));
    accounts[index]->balance += dep_amount;
    accounts[index]->transaction_tracter += dep_amount;
    pthread_mutex_unlock(&(accounts[index]->ac_lock));
}

void transfer_funds(int index1, int index2, command_line *cmd)
{
    double transfer_amount;
    sscanf(cmd->command_list[4],"%lf", &transfer_amount);

    // critical section add mutex lock and unlock but
    // seperate to avoid deadlocks

    pthread_mutex_lock(&(accounts[index2]->ac_lock));
    accounts[index2]->balance += transfer_amount; // give money to dest
    pthread_mutex_unlock(&(accounts[index2]->ac_lock));

    pthread_mutex_lock(&(accounts[index1]->ac_lock));
    accounts[index1]->balance -= transfer_amount; // take moiney from src
    accounts[index1]->transaction_tracter += transfer_amount; // add to src trans tracker
    pthread_mutex_unlock(&(accounts[index1]->ac_lock));
}