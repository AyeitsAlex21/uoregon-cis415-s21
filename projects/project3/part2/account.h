#ifndef ACCOUNT_H_
#define ACCOUNT_H_

// part 3 read how many lines there are give each thread an wqual amount of 
// transactions
#include <pthread.h>
typedef struct
{
	char account_number[17];
	char password[9];
    double balance;
    double reward_rate;
    
    double transaction_tracter;

    char out_file[64];

    pthread_mutex_t ac_lock;
}account;

#endif /* ACCOUNT_H_ */