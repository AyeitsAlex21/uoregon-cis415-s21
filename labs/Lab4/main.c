#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

void script_print (pid_t* pid_ary, int size);

int main(int argc,char*argv[])
{
	pid_t cpid = 1;
	int n = -1;
	char *args[] = {"./iobound", "-seconds", "5", 0};
	if (argc == 1)
	{
		printf ("Wrong number of argument\n");
		exit (0);
	}
	for(int i = 0; i < argc; i++)
	{
		if(strcmp(argv[i], "./lab4") && argc == i + 1)
		{
			if(sscanf(argv[i], "%d", &n) != 1)
			{
				printf("Invalid argument\n");
				exit(0);
			}
		}
	}
	/*
	*	TODO
	*	#1	declear child process pool
	*	#2 	spawn n new processes
	*		first create the argument needed for the processes
	*		for example "./iobound -seconds 10"
	*	#3	call script_print
	*	#4	wait for children processes to finish
	*	#5	free any dynamic memories
	*/

	// diff between cpid and pid
	// top shows all proccesses
	// kill is powerful signal to kill processes
	// use perror in project
	pid_t *pid_ary = (pid_t *)malloc(sizeof(pid_t) * n);
	freopen("log.txt", "w+", stdout);

	for(int i = 0; i < n; i++)
	{
		if(cpid > 0)
		{
			cpid = fork();
			pid_ary[i] = cpid;
		}
		if(cpid < 0)
		{
			perror("fork");
			exit(EXIT_FAILURE);
		}
		if(cpid == 0)
		{
			execvp("./iobound", args);
		}
	}

	script_print(pid_ary, n);	

	for(int i = 0; i < n; i++)
	{
		waitpid(pid_ary[i], NULL, 0);
	}

	free(pid_ary);

	return 0;
}


void script_print (pid_t* pid_ary, int size)
{
	FILE* fout;
	fout = fopen ("top_script.sh", "w");
	fprintf(fout, "#!/bin/bash\ntop");
	for (int i = 0; i < size; i++)
	{
		fprintf(fout, " -p %d", (int)(pid_ary[i]));
	}
	fprintf(fout, "\n");
	fclose (fout);

	char* top_arg[] = {"gnome-terminal", "--", "bash", "top_script.sh", NULL};
	pid_t top_pid;

	top_pid = fork();
	{
		if (top_pid == 0)
		{
			if(execvp(top_arg[0], top_arg) == -1)
			{
				perror ("top command: ");
			}
			printf("%d\n", top_pid);
			exit(0);
		}
	}
}


