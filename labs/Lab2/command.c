#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include "command.h"

void listDir()
{
    char cwd[300];
    DIR *dir_ptr;
    struct dirent *dir_struc;

    getcwd(cwd, sizeof(cwd));
    dir_ptr = opendir(cwd);

    while( (dir_struc = readdir(dir_ptr)) != NULL)
    {
        write(1, dir_struc->d_name, strlen(dir_struc->d_name));
        write(1, " ", 1);
    }
    write(1, "\n", 1);
    
    closedir(dir_ptr);
}

void showCurrentDir()
{
    char cwd[300];

    getcwd(cwd, sizeof(cwd));
    write(1, cwd, strlen(cwd));
    write(1, "\n", 1);
}

void makeDir(char *dirName)
{   
    struct stat st = {0};

    if(stat(dirName, &st) == -1)
        mkdir(dirName, S_IRWXU);
    else
    {
        write(1, "mkdir: cannot create directory ‘", 34);
        write(1, dirName, strlen(dirName));
        write(1, "': File exists\n", 15);
    }
}

void changeDir(char *dirName)
{
    struct stat st = {0};

    if(stat(dirName, &st) == 0)
        chdir(dirName);
    
    else
    {
        write(1, "bash: cd: ", 10);
        write(1, dirName, strlen(dirName));
        write(1, ": No such file or directory\n", 28);
    }
}
// file descriptor is intergerlinked to file
// use 666
//call open with create to replace
void copyFile(char *sourcePath, char *destinationPath)
{
    DIR *dir_ptr;
    struct dirent *dir_struc;
    struct stat st = {0};

    if(stat(sourcePath, &st) == 0)
    {
        char buf[1024];
        ssize_t count;
        int fd_read;
        int fd_write;
        dir_ptr = opendir(sourcePath);

        if(dir_ptr != NULL)
        {
            mkdir(destinationPath, S_IRWXU);
            
            while( (dir_struc = readdir(dir_ptr)) != NULL)
            {
                char dest[300];
                char src[300];

                strcpy(src, sourcePath);
                strcat(src, "/");
                strcat(src, dir_struc->d_name);
                fd_read = open(src, O_RDONLY);

                strcpy(dest, destinationPath);
                strcat(dest, "/");
                strcat(dest, dir_struc->d_name);

                printf("\n%s\n", dest);
                fd_write = open(dest, O_CREAT | O_WRONLY, 0664);
                printf("\n%d\n", fd_read);

                while( (count = read(fd_read, buf, 1024)) > 0)
                    write(fd_write, buf, count);
            }
        }
        // source is a file
        else
        {
            fd_read = open(sourcePath, O_RDONLY);
            fd_write = open(destinationPath, O_CREAT | O_WRONLY, 0664);

            while( (count = read(fd_read, buf, 1024)) > 0)
                write(fd_write, buf, count);
        }
    }
    else
    {
        write(1, "bash: cd: ", 10);
        write(1, sourcePath, strlen(sourcePath));
        write(1, ": No such file or directory\n", 28);
    }
}

void moveFile(__attribute__((unused)) char *sourcePath,__attribute__((unused)) char *destinationPath)
{
    
}

void deleteFile(char *filename)
{
    if(remove(filename) != 0)
    {
        write(1, "rm: cannot remove '", 19);
        write(1, filename, strlen(filename));
        write(1, "': No such file or directory\n", 29);
    }
}

void displayFile(char *filename)
{
    int fd_read = open(filename, O_RDONLY);
    char buf[1024];
    ssize_t count;

    if(fd_read != -1)
    {
        while( (count = read(fd_read, buf, 1024)) > 0)
        {
            write(1, buf, count);
        }
    }
    else
    {
        write(1, "cat: ", 5);
        write(1, filename, strlen(filename));
        write(1, ": No such file or directory\n", 28);
    }
}